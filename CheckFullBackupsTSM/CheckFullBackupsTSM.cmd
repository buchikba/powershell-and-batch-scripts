@echo on
FOR /F "tokens=1-8 delims=./- " %%a in ('echo %date%') do (set mydate=%%a.%%b.%%c)

set report_dir=C:\full_backup_reports
set result=%report_dir%\%mydate%_FULL.BACKUP.ON.TSM.txt
set db2adutlDir=C:\IBM\SQLLIB\BIN

echo ==================== CURRENT DATE: %mydate% ==================== >%result%
%db2adutlDir%\db2adutl.exe query full >>%result%

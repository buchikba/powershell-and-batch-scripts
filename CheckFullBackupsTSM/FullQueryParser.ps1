﻿$scriptroot = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath('.\')
$report = "$scriptroot\report.txt"
$nl = [Environment]::NewLine
(Get-Date).tostring("dd-MM-yyyy-HH-mm-ss") > $report
if (!(Test-Path -Path $scriptroot\logs)) { New-Item -ItemType dir $scriptroot\logs }
Get-Content $scriptroot\inventory.txt | ? { $_.Trim() -ne "" } | ForEach-Object {
    if ($_ -like 'hostname*') { $hostname = $_.Substring($_.IndexOf('=')+1) }
	if ($_ -like 'ip*') { $ip = $_.Substring($_.IndexOf('=')+1) }
	if ($_ -like 'username*') { $username = $_.Substring($_.IndexOf('=')+1) }
	if ($_ -like 'password*') { $password = $_.Substring($_.IndexOf('=')+1) }
	if (($hostname -ne $null) -and ($ip -ne $null) -and ($username -ne $null) -and ($password -ne $null))
	{
    $path = "\\" + $ip + "\full_backup_reports"
	net use $path $password /USER:$username 2>&1>$null
	"$nl------------------------$nl$hostname$nl------------------------$nl" >> $report
	$latestlog = Get-ChildItem -Path $path"\*FULL.BACKUP*.txt" | Sort CreationTime -Descending | Select-Object -First 1
#	$copiedlog = Copy-Item $latestlog $scriptroot\logs\$hostname" backup.log" -PassThru
#	Get-Content $copiedlog | ? { $_.Trim() -ne "" } | Select-String "FULL DATABASE BACKUP" -context 1,4 | ? {$_.Context.PostContext[0] -match "^\s+\d"} | ForEach-Object {$_.Context.PreContext + $nl + $_.Context.PostContext +$nl >> $scriptroot/report.txt}
#	Для создания отчета без вытаскивания логов на локальный компьютер нужно закоментировать две верхние строки и раскомментировать строку внизу
	Get-Content $latestlog | ? { $_.Trim() -ne "" } | Select-String "FULL DATABASE BACKUP" -context 1,4 | ? {$_.Context.PostContext[0] -match "^\s+\d"} | ForEach-Object {$_.Context.PreContext + $nl + $_.Context.PostContext +$nl >> $scriptroot/report.txt}
	net use $path /delete 2>&1>$null
	$hostname = $null
	$ip = $null
	$username = $null
	$password = $null
	$path = $null
	}
	}
(Get-Date).tostring("dd-MM-yyyy-HH-mm-ss") >> $scriptroot\report.txt
